FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
ENV APP_PROJETO "app_ojs"
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

FROM alpine:3.11 AS develop

LABEL maintainer="Rafael Roque de Mello <lucas.diedrich@gmail.com>"
WORKDIR /var/www/html

ENV COMPOSER_ALLOW_SUPERUSER=1  \
    SERVERNAME="localhost"      \
    HTTPS="on" \
    OJS_VERSION=$ref       \
    PACKAGES="supervisor dcron apache2 apache2-ssl apache2-utils php7 php7-fpm php7-cli php7-apache2 php7-session php7-pgsql php7-zlib \
             php7-json php7-phar php7-openssl php7-curl php7-simplexml php7-mbstring php7-xmlwriter  php7-tokenizer php7-pdo_pgsql php7-mcrypt php7-ctype php7-pdo \
             php7-gd php7-xml php7-dom php7-iconv curl nodejs npm git php7-bcmath php7-ctype  php7-fileinfo php7-xmlreader php7-zip php7-ldap php7-opcache postgresql-client postgis gdal zip unzip \
             ca-certificates tzdata curl openssh-client libltdl shadow util-linux pciutils   usbutils   coreutils  binutils    findutils  grep  bash bash-completion gettext"

RUN apk add --update --no-cache $PACKAGES && \
     rm -rf /var/lib/apt/lists/* && \
     #echo "America/Sao_Paulo" >  /etc/timezone  && \
     cp /usr/share/zoneinfo/Brazil/East /etc/localtime && \
     apk del tzdata && \
     curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD serpro-ca/root_1.crt /usr/local/share/ca-certificates/root_1.crt
ADD serpro-ca/root_2.crt /usr/local/share/ca-certificates/root_2.crt
ADD serpro-ca/root_3.crt /usr/local/share/ca-certificates/root_3.crt
ADD serpro-ca/icpbrasilv10.crt /usr/local/share/ca-certificates/icpbrasilv10.crt
ADD serpro-ca/serprossl.crt /usr/local/share/ca-certificates/serprossl.crt
ADD icmbio-ca/CA_ICMBIO.crt /usr/local/share/ca-certificates/CA_ICMBIO.crt

RUN update-ca-certificates

# Create directories
RUN mkdir -p /var/www/files /run/apache2  /run/supervisord/ && \
    chown -R apache:apache /var/www/* && \
    # Prepare crontab
    # Prepare httpd.conf
    sed -i -e '\#<Directory />#,\#</Directory>#d' /etc/apache2/httpd.conf && \
    sed -i -e "s/^ServerSignature.*/ServerSignature Off/" /etc/apache2/httpd.conf
    # Clear the image
#RUN apk del --no-cache nodejs git

COPY conf/files/ /

EXPOSE 80 443

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]


FROM develop AS build

FROM develop AS production
